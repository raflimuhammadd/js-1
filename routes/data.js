const bodyParser = require('body-parser');
const express = require('express');
const router = express.Router();
const db = require('../config/mysql');

router.get('/', (req, res, next) => {
    var sql = "SELECT * FROM data";
    db.query(sql, (err, result) => {
        if (err) throw err;
        res.status(200).json({
            message: "Get Data",
            data: result
        });
    })
});

router.post('/add', (req, res, next) => {
    const nim = req.body.nim;
    const nama = req.body.nama;
    const jurusan = req.body.jurusan;
    var sql = "INSERT INTO data (nim, nama, jurusan) VALUES ('" + nim + "', '" + nama + "', '" + jurusan + "')";

    db.query(sql, (err, result) => {
        if (err) throw err;
        res.status(200).json({
            message: "Data Found",
            data: result
        });
    })
})

router.get('/:nim', (req, res, next) => {
    const nim = req.params.nim;
    var sql = "SELECT * FROM data WHERE nim=" + nim;
    db.query(sql, (err, result) => {
        if (err) throw err;
        res.status(200).json({
            message: "Data Found",
            data: result
        });
    })
})

router.put('/update', (req, res, next) => {
    const nim = req.body.nim;
    const nama = req.body.nama;
    const jurusan = req.body.jurusan;
    var sql = "UPDATE data SET nama='" + nama + "', jurusan='" + jurusan + "' WHERE nim=" + nim;
    db.query(sql, (err, result) => {
        if (err) throw err;
        res.status(200).json({
            message: "Data Updated",
            data: result
        });
    });
});

router.delete('/del', (req, res, next) => {
    const nim = req.body.nim;
    var sql = "DELETE FROM data WHERE nim=" + nim;
    db.query(sql, (err, result) => {
        if (err) throw err;
        res.status(200).json({
            message: "Data Deleted",
            data: result
        });
    });
});

module.exports = router;