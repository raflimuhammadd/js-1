const express = require('express');
const app = express();
const dataRoute = require('./routes/data');
const morgan = require('morgan');
const bodyParser = require('body-parser');

app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use('/data', dataRoute);


module.exports = app;